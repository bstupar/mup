$(document).ready(function() {
    
    $('#proizvodjac').change(function() {
    	$.ajax({
    		type: 'POST',
    		url: 'ajax',
    		dataType: 'json',
    		data: {id_alkometra: $('#proizvodjac').val(), token: $('input[name=_token]').val()},
    		beforeSend: function() {
    			$('#ajaxModal').modal('show');
    		},
    		complete: function() {
    			$('#ajaxModal').modal('hide');
    		},
    		success: function(data) {   
    			// we need some default option, so .. 			
    			var output = '<option>Izabrati model</option>';
    			//generate new option list with fetched data
				for (var i = 0, len = data.length; i < len; ++i) {
					output += '<option value="' + data[i].id + '">' + data[i].model + '</option>';
				}
				//clear old select options and append new
				$('#model').empty();
				$('#model').append(output);
    		}
    	});
    });

    $('#godina_proizvodnje').datepicker({
	    format: "yyyy",
	    startView: 1,
	    minViewMode: 2,
	    orientation: "top left",
	    autoclose: true,
	});

	$('#kalibracija').datepicker({
		format: "dd-MM-yyyy",
		todayBtn: "linked",
		language: "sr",
		orientation: "top left",
		autoclose: true,
		todayHighlight: true,
	});

    $('#drzavni_zig').datepicker({
		format: "dd-MM-yyyy",
		todayBtn: "linked",
		language: "sr",
		orientation: "top left",
		autoclose: true,
		todayHighlight: true,
	});

    $('input[name=troskovi_kalibracije]').numeric({
    	negative: false,
    	decimalPlaces: 2,
    });
    $('input[name=troskovi_servisa]').numeric({
    	negative: false,
    	decimalPlaces: 2,
    });

});
