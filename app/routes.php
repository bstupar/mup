<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('alkometri');
});
Route::resource('user', 'UsersController');
Route::get('login', array('uses' => 'UsersController@getLogin'));
Route::post('login', array('uses' => 'UsersController@doLogin'));
Route::get('logout', array('uses' => 'UsersController@doLogout'));
Route::post('user/{id}/changePass', array('uses' => 'UsersController@changePass'));

Route::get('alkometri', array('uses' => 'AlkometriController@index'));
Route::get('alkometri/dodaj', array('uses' => 'AlkometriController@create'));
Route::post('alkometri/dodaj', array('uses' => 'AlkometriController@store'));
Route::get('alkometri/ajax', array('uses' => 'AlkometriController@getAjax'));
Route::post('alkometri/ajax', array('uses' => 'AlkometriController@postAjax'));
Route::get('alkometri/proizvodjaci', array('uses' => 'AlkometriController@createProizvodjac'));
Route::post('alkometri/proizvodjaci', array('uses' => 'AlkometriController@storeProizvodjac'));
Route::get('alkometri/proizvodjaci/{id}', array('uses' => 'AlkometriController@editProizvodjac'));
Route::post('alkometri/proizvodjaci/{id}', array('uses' => 'AlkometriController@storeModel'));
Route::put('alkometri/proizvodjaci/edit/{id}', array('uses' => 'AlkometriController@updateProizvodjac'));
Route::get('alkometri/{id}', array('uses' => 'AlkometriController@edit'));
Route::put('alkometri/{id}', array('uses' => 'AlkometriController@update'));

Route::get('uprava', array('uses' => 'PupravaController@index'));
Route::post('uprava', array('uses' => 'PupravaController@store'));
Route::get('uprava/{id}', array('uses' => 'PupravaController@edit'));
Route::put('uprava/edit/{id}', array('uses' => 'PupravaController@update'));
Route::post('uprava/{id}', array('uses' => 'PupravaController@storeStanica'));

Route::get('test', function() {

	$sql_details = array(
		'user' => Config::get('database.connections.mysql.username'),
		'pass' => Config::get('database.connections.mysql.password'),
		'db'   => Config::get('database.connections.mysql.database'),
		'host' => Config::get('database.connections.mysql.host'),
	);

	// DB table to use
	$table = "alkometri";

	// Table's primary key
	$primaryKey = "id";

	$columns = array(
		array( "db" => "alkometri.id"           			, "dt" => "id",
            'formatter' => function($d, $row) {
                return "<a href=". URL::to('alkometri/' . $d).">". $d ."</a>";
            }),
		array( "db" => "alkoproizvodjac.ime_alkometra"		, "dt" => "ime_alkometra"),
		array( "db" => "alkomodel.model"        			, "dt" => "model" ),
		array( "db" => "alkometri.serijski_broj"			, "dt" => "serijski_broj"),
        array( "db" => "alkometri.inventarski_broj"			, "dt" => "inventarski_broj"),
        array( "db" => "alkometri.godina_proizvodnje"		, "dt" => "godina_proizvodnje"),
        array( "db" => "alkometri.kalibracija"				, "dt" => "kalibracija"),
        array( "db" => "alkometri.drzavni_zig"				, "dt" => "drzavni_zig"),
        array( "db" => "alkometri.troskovi_kalibracije"		, "dt" => "troskovi_kalibracije"),
        array( "db" => "alkometri.troskovi_servisa"			, "dt" => "troskovi_servisa"),
        array( "db" => "alkometri.stanje"					, "dt" => "stanje",
        	'formatter' => function($d, $row) {
        		if ($d == 0) {
        			return "Није на стању";
        		} elseif ($d == 1) {
        			return "На стању";
        		}
        	}),
        array( "db" => "alkometri.status"					, "dt" => "status"),
        array( "db" => "alkometri.napomena"					, "dt" => "napomena"),
        array( "db" => "policijska_stanica.ime_pstanica"	, "dt" => "ime_pstanica")
	);

    $join = array(
        "join alkoproizvodjac on alkometri.alkoproizvodjac_id = alkoproizvodjac.id ",
        "join alkomodel on alkometri.alkomodel_id = alkomodel.id ",
        "join policijska_stanica on alkometri.pstanica_id = policijska_stanica.id "
    );

	$q = \Libraries\Ssp::simple(Input::all(), $sql_details, $table, $primaryKey, $columns, $join);
	return Response::json($q);

});
Route::get('seed/{num}', function($num) {
    if (!$num) {
        Libraries\Seed::alkometri(10);
    } else {
        Libraries\Seed::alkometri($num);
    }
});

