<?php

class Alkometri extends \Eloquent {

	protected $table = 'alkometri';

	public static function findById($id)
	{
		return DB::table('alkometri')
                    ->leftJoin('alkoproizvodjac', 'alkometri.alkoproizvodjac_id', '=', 'alkoproizvodjac.id')
                    ->leftJoin('alkomodel', 'alkometri.alkomodel_id', '=', 'alkomodel.id')
                    ->leftJoin('policijska_stanica', 'alkometri.pstanica_id', '=', 'policijska_stanica.id')
                    ->select('alkometri.*', 'alkoproizvodjac.ime_alkometra', 'alkomodel.model', 'policijska_stanica.ime_pstanica')
                    ->where('alkometri.id', '=', $id)
                    ->first();
	}

	public static function getLimit($limit)
	{
		return DB::table('alkometri')
                    ->join('alkoproizvodjac', 'alkometri.alkoproizvodjac_id', '=', 'alkoproizvodjac.id')
                    ->join('alkomodel', 'alkometri.alkomodel_id', '=', 'alkomodel.id')
                    ->join('policijska_stanica', 'alkometri.pstanica_id', '=', 'policijska_stanica.id')
                    ->select('alkometri.*', 'alkoproizvodjac.ime_alkometra', 'alkomodel.model', 'policijska_stanica.ime_pstanica')
                    ->take($limit)
                    ->get();
	}
	
}
 