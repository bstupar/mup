<?php

namespace Libraries;

use \Illuminate\Database\Eloquent;
use Alkoproizvodjac;
use Alkomodel;
use Pstanica;
use Alkometri;

class Seed {

    // just some static variables declaration
    private static $proizvodjaci = '';
    private static $modeli = '';
    private static $pstanica = '';

    /**
     * Generate $num uredjaja u bazi, jerbo testiranje ? :P
     *
     * @param $num  int value
     */
    public static function alkometri($num)
    {
        // get table data so we can play with it in our class
        static::$proizvodjaci = Alkoproizvodjac::all()->toArray();
        static::$modeli = Alkomodel::all()->toArray();
        static::$pstanica = Pstanica::all()->toArray();

        for ($i = 1; $i <= $num; $i++) {

            // set variables
            setlocale(LC_TIME, 'sr', 'sr_SP', 'rs');
            $randProizvodjac = self::randProizvodjac();
            $randModel = self::randModel($randProizvodjac);
            $randPstanica = self::randPstanica();
            $time = rand( strtotime('Jan 01 2010'), strtotime('May 01 2015'));
            //$date = date('d-F-Y', $time);
            $date = strftime('%d-%B-%Y', $time);
            $status = array('Сервис','Редован сервис','На коришћењу','За расход');

            // creation magic :)
            $alkometar = new Alkometri();
            $alkometar->alkoproizvodjac_id = $randProizvodjac;
            $alkometar->alkomodel_id = $randModel;
            $alkometar->serijski_broj = rand(100, 1000);
            $alkometar->inventarski_broj = rand(1, 1000) . '/' . rand(10, 15);
            $alkometar->godina_proizvodnje = rand(2010, 2015);
            $alkometar->kalibracija = $date;
            $alkometar->drzavni_zig = $date;
            $alkometar->troskovi_kalibracije = rand(0, 10000);
            $alkometar->troskovi_servisa = rand(0, 10000);
            $alkometar->stanje = (int) rand(0, 1);
            $alkometar->status = $status[rand(1, count($status)) -1];
            $alkometar->napomena = 'generated ' . $i;
            $alkometar->pstanica_id = $randPstanica;
            $alkometar->save();
        }
    }

    /**
     * Get random id proizvodjaca
     *
     * @return id proizvodjaca
     */
    private static function randProizvodjac()
    {
        // -1 is for offseting by 1 value. Arrays start from 0, and counting arrays starts from 1
        return static::$proizvodjaci[rand(1, count(static::$proizvodjaci)) -1]['id'];
    }

    /**
     * Get random ID modela na osnovu proizvodjaca
     *
     * @param $proizvodjac  id random proizvodjaca na osnovu koga se radi pretraga modela
     * @return id modela na osnovu id proizvodjaca
     */
    private static function randModel($proizvodjac)
    {
        // don't try to understand this .. it just works .. for some reason
        $num = array();
        for ($i = 0; $i < count(static::$modeli); $i++) {
            if (static::$modeli[$i]['id_alkometra'] == $proizvodjac) {
                $num[] = static::$modeli[$i]['id'];
            }
        }

        return $num[rand(1, count($num)) - 1];
    }

    /**
     * Get random id policijske stanice
     *
     * @return id pstanica
     */
    private static function randPstanica()
    {
        // -1 is for offseting by 1 value. Arrays start from 0, and counting arrays starts from 1
        return static::$pstanica[rand(1, count(static::$pstanica)) -1]['id'];
    }
}