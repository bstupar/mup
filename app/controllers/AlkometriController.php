<?php

class AlkometriController extends \BaseController {

	protected $layout = 'main';
	
	public function __construct()
	{
		$this->beforeFilter('auth');
	}

    /**
     * Prikaz index stranice
     * GET /alkometri
     *
     * @return Response
     */
    public function index()
    {
        // var_dump(Alkometri::all()->toArray());
        // dd(Alkometri::getLimit(10));
        $data['alkometri'] = Alkometri::getLimit(10);
        return View::make('alkometri.alko', $data);
    }

    /**
     * Prikaz stranice za dodavanje
     * GET /alkometri/dodaj
     *
     * @return Response
     */
    public function create()
    {
        $data['proizvodjaci'] = Alkoproizvodjac::all()->toArray();
        $data['pstanice'] = Pstanica::all()->toArray();
        return View::make('alkometri.add', $data);
    }

    /**
     * Creates new alko device
     * POST /alkometri/dodaj
     *
     * @return Response
     */
    public function store()
    {
        
        $rules = array(
            'proizvodjac'           => 'required|numeric',
            'godina_proizvodnje'    => 'required|numeric',
            'stanje'                => 'required|numeric',
            'model'                 => 'required|numeric',
            'kalibracija'           => 'required',
            'status'                => 'required',
            'serijski_broj'         => 'required',
            'drzavni_zig'           => 'required',
            'inventarski_broj'      => 'required',
            'policijska_stanica_id' => 'required|numeric',
            'troskovi_kalibracije'  => '',
            'troskovi_servisa'      => '',
            'namepomena'            => ''
            );
        $attributeNames = array(
            'proizvodjac'           => 'Произвођач',
            'godina_proizvodnje'    => 'Година производње',
            'stanje'                => 'Стање',
            'model'                 => 'Модел',
            'kalibracija'           => 'Датум калибрације',
            'status'                => 'Статус',
            'serijski_broj'         => 'Серијски број',
            'drzavni_zig'           => 'Државни жиг',
            'inventarski_broj'      => 'Инвентарски број',
            'policijska_stanica_id' => 'Полицијска станица',
            'troskovi_kalibracije'  => 'Трошкови калибрације',
            'troskovi_servisa'      => 'Трошкови сервиса',
            'napomena'              => 'Напомена'
            );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return Redirect::to('alkometri/dodaj')
                        ->withErrors($validator)
                        ->withInput();
        } else {

            $alkometar = new Alkometri();
            $alkometar->alkoproizvodjac_id = Input::get('proizvodjac');
            $alkometar->alkomodel_id = Input::get('model');
            $alkometar->serijski_broj = Input::get('serijski_broj');
            $alkometar->inventarski_broj = Input::get('inventarski_broj');
            $alkometar->godina_proizvodnje = Input::get('godina_proizvodnje');
            $alkometar->kalibracija = Input::get('kalibracija');
            $alkometar->drzavni_zig = Input::get('drzavni_zig');
            $alkometar->troskovi_kalibracije = Input::get('troskovi_kalibracije');
            $alkometar->troskovi_servisa = Input::get('troskovi_servisa');
            $alkometar->stanje = (int) Input::get('stanje');
            $alkometar->status = Input::get('status');
            $alkometar->napomena = Input::get('napomena');
            $alkometar->pstanica_id = Input::get('policijska_stanica_id');
            $alkometar->save();

            Session::flash('message', 'Uspesno dodat alkometar');
            Session::flash('alert-class', 'alert-success');
            return Redirect::to('alkometri');
            // dd(Input::all());
        }
    }

    /**
     * Edit $id device
     * GET /alkometri/$id
     *
     * @param $id
     * @return Response
     */
    public function edit($id)
    {
        $data['proizvodjaci'] = Alkoproizvodjac::all()->toArray();
        $data['pstanice'] = Pstanica::all()->toArray();
        $data['alkometar'] = Alkometri::findById($id);
        return View::make('alkometri.edit', $data);
    }

    /**
     * Updates $id device
     * PUT /alkometri/$id
     *
     * @param $id
     */
    public function update($id)
    {
        $rules = array(
            'proizvodjac'           => 'required|numeric',
            'godina_proizvodnje'    => 'required|numeric',
            'stanje'                => 'required|numeric',
            'model'                 => 'required|numeric',
            'kalibracija'           => 'required',
            'status'                => 'required',
            'serijski_broj'         => 'required',
            'drzavni_zig'           => 'required',
            'inventarski_broj'      => 'required',
            'policijska_stanica_id' => 'required|numeric',
            'troskovi_kalibracije'  => '',
            'troskovi_servisa'      => '',
            'namepomena'            => ''
            );
        $attributeNames = array(
            'proizvodjac'           => 'Произвођач',
            'godina_proizvodnje'    => 'Година производње',
            'stanje'                => 'Стање',
            'model'                 => 'Модел',
            'kalibracija'           => 'Датум калибрације',
            'status'                => 'Статус',
            'serijski_broj'         => 'Серијски број',
            'drzavni_zig'           => 'Државни жиг',
            'inventarski_broj'      => 'Инвентарски број',
            'policijska_stanica_id' => 'Полицијска станица',
            'troskovi_kalibracije'  => 'Трошкови калибрације',
            'troskovi_servisa'      => 'Трошкови сервиса',
            'napomena'              => 'Напомена'
            );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return Redirect::to('alkometri/dodaj')
                        ->withErrors($validator)
                        ->withInput();
        } else {

            $alkometar = Alkometri::find($id);
            $alkometar->alkoproizvodjac_id = Input::get('proizvodjac');
            $alkometar->alkomodel_id = Input::get('model');
            $alkometar->serijski_broj = Input::get('serijski_broj');
            $alkometar->inventarski_broj = Input::get('inventarski_broj');
            $alkometar->godina_proizvodnje = Input::get('godina_proizvodnje');
            $alkometar->kalibracija = Input::get('kalibracija');
            $alkometar->drzavni_zig = Input::get('drzavni_zig');
            $alkometar->troskovi_kalibracije = Input::get('troskovi_kalibracije');
            $alkometar->troskovi_servisa = Input::get('troskovi_servisa');
            $alkometar->stanje = (int) Input::get('stanje');
            $alkometar->status = Input::get('status');
            $alkometar->napomena = Input::get('napomena');
            $alkometar->pstanica_id = Input::get('policijska_stanica_id');
            $alkometar->save();

            Session::flash('message', 'Uspesno izmenjen alkometar');
            Session::flash('alert-class', 'alert-success');
            return Redirect::to('alkometri/' . $id);
        }
    }


    public function delete($id)
    {

    }

    public function postAjax()
    {
        if (Session::token() != Input::get('token')) {
            return Response::json(array(
                'msg' => 'Nedozvoljeni pristup'));
        } else {

            return Alkomodel::where('id_alkometra', Input::get('id_alkometra'))->get()->toJson();
        }
    }

    public function getAjax()
    {
        
    }

    /**
     * Prikaz create stranice za proizvodjaca
     * GET /alkometri/proizvodjaci
     * 
     * @return Response
     */
    public function createProizvodjac()
    {
        $data['proizvodjaci'] = Alkoproizvodjac::all();
        return View::make('alkometri.proizvodjac', $data);
    }

    /**
     * Snimanje novog proizvodjaca
     * POST /alkometri/proizvodjaci/
     * 
     * @return Response
     */
    public function storeProizvodjac()
    {
        // process input and check the rules
        $rules = array(
            'ime_alkometra' => 'required|unique:alkoproizvodjac,ime_alkometra'
        );
        $attributeNames = array(
            'ime_alkometra' => "Име произвођача"
            );
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return Redirect::to('alkometri/proizvodjaci')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $proizvodjac = new Alkoproizvodjac();
            $proizvodjac->ime_alkometra = Input::get('ime_alkometra');
            $proizvodjac->save();

            Session::flash('message', 'Успешно додат произвођач');
            Session::flash('alert-class', 'alert-success');
            return Redirect::to('alkometri/proizvodjaci');
        }
    }

    /**
     * Prikaz stranice za izmenu proizvodjaca i dodavanje modela
     * GET /alkometri/proizvodjaci/{id}
     * 
     * @param  int      $id      ID proizvodjaca
     * @return Response
     */
    public function editProizvodjac($id)
    {
        $data['proizvodjac'] = Alkoproizvodjac::find($id);
        $data['modeli'] = Alkomodel::where('id_alkometra', $id)->get();
        return View::make('alkometri.proizvodjacedit', $data);
    }

    /**
     * Editovanje proizvodjaca
     * PUT /alkometri/proizvodjaci/edit/{id}
     * 
     * @param  int      $id     ID proizvodjaca
     * @return Response
     */
    public function updateProizvodjac($id)
    {
        $rules = array(
            'ime_alkometra' => 'required'
            );
        $attributeNames = array(
            'ime_alkometra' => "Име произвођача"
            );
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $alkometar = Alkoproizvodjac::find($id);
            $alkometar->ime_alkometra = Input::get('ime_alkometra');
            $alkometar->save();

            Session::flash('message', 'Успешно промењено име');
            Session::flash('alert-class', 'alert-success');
            return Redirect::to('alkometri/proizvodjaci/' . $id);

        }
    }

    /**
     * Snimanje novog modela alkometra
     * POST /alkometri/proizvodjaci/{id}
     * 
     * @param  int      $id     ID alkometra
     * @return Response
     */
    public function storeModel($id)
    {
        $rules = array(
            'model' => 'required|unique:alkomodel,model'
            );
        $attributeNames = array(
            'model' => 'Модел алкометра'
            );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $model = new Alkomodel();
            $model->model = Input::get('model');
            $model->id_alkometra = $id;
            $model->save();

            Session::flash('message', 'Успешно додат модел');
            Session::flash('alert-class', 'alert-success');
            return Redirect::to('alkometri/proizvodjaci/' . $id);
        }
    }

}