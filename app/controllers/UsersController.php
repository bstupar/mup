<?php

class UsersController extends \BaseController {

    protected $layout = 'main';

    public function __construct()
	{
		//$this->beforeFilter('auth', array('except' => array('getLogin', 'doLogin')));
	}

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		return View::make('users.index')->with('users', $users);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::user()->sudo === 1) {
			// set the rules and validate
			$rules = array(
				'full_name' => 'required',
				'email'		=> 'required|email|unique:users,email',
				'password'	=> 'required|alpha_dash',
				're_password' => 'required|same:password'
				);
			$attributeNames = array(
				'full_name' => 'Име и презиме',
				'email' => 'E-пошта',
				'password' => 'Шифра',
				're_password' => 'Поновљена шифра'
				);
			$validator = Validator::make(Input::all(), $rules);
			$validator->setAttributeNames($attributeNames);

			//process result - error or create user
			if ($validator->fails()) {
				return Redirect::to('user/create')
					->withErrors($validator)
					->withInput(Input::except('password', 're_password'));
			} else {
				$user = new User;
				$user->full_name 	= Input::get('full_name');
				$user->email 		= Input::get('email');
				$user->password 	= Hash::make(Input::get('password'));
				$user->save();

				//redirect to main page
				Session::flash('message', 'Успешно регистрован корисник');
				Session::flash('alert-class', 'alert-success');
				return Redirect::to('user');
				
			}
		}
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		if (isset($user)) {
			return View::make('users.show')
				->with('user', $user);
		} else {
			Session::flash('message', 'Тражени корисник није пронађен');
			Session::flash('alert-class', 'alert-warning');
			return Redirect::to('user');
		}
		
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// dd(Auth::user()->sudo);
		if (Auth::user()->id === (int) $id or Auth::user()->sudo === 1 ) {
			$user = User::find($id);
			return View::make('users.edit')->with('user', $user);
		} else {
			Session::flash('message', 'Немате одговарајуће пермисије да измените кориснички налог');
			Session::flash('alert-class', 'alert-warning');
			return Redirect::to('user');
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//only account owner or admin can change password
		if (Auth::user()->id === (int) $id or Auth::user()->sudo === 1) {
			
			//TODO:: add option to change email/ full name


		} else {
			// permission error
			Session::flash('message', 'Немате одговарајуће пермисије да измените кориснички налог');
			Session::flash('alert-class', 'alert-danger');
			return Redirect::to('user');
		}
	}

	/**
	 * Changes password of selected user
	 * POST /user/{id}/changePass
	 * 
	 * @param  int $id 
	 * @return Response
	 */
	public function changePass($id)
	{
		//only account owner or admin can change password
		if (Auth::user()->id === (int) $id OR Auth::user()->sudo === 1) {
			$rules = array(
				'new_password' => 'required|alpha_dash',
				'new_pass_repeat' => 'required|same:new_password'
				);
			if (Auth::user()->id === $id) {
				$rules['old_password'] = 'required|alpha_dash';
			}

			$validator = Validator::make(Input::all(), $rules);

			// process data validation
			if ($validator->fails()) {
				return Redirect::to('user')->withErrors($validator);

			} else {

				$credentials = array(
					'email' => Auth::user()->email,
					'password' => Input::get('old_password')
					);

				// check if old password is matching the user
				// we don't wont to change some other users password
				if (Auth::validate($credentials)) {

					// save new password in DB
					$user = new User;
					$user->password = Hash::make(Input::get('new_password'));
					$user->save;

					Session::flash('message', 'Успешно промењена шифра');
					Session::flash('alert-class', 'alert-success');
					return Redirect::to('user');
				
				} else {
					// handle error if data didn't match
					Session::flash('error', 'Унети подаци нису исправни');
					Session::flash('alert-class', 'alert-danger');
					return Redirect::to('user');
				}

			}
		} else {
			// permission error
			Session::flash('message', 'Немате одговарајуће пермисије да измените кориснички налог');
			Session::flash('alert-class', 'alert-danger');
			return Redirect::to('user');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (Auth::user()->sudo ===1) {
			$user = User::find($id);
			$user->delete();

			Session::flash('message', 'Корисник ' . $user->full_name . ' успешно обрисан');
			Session::flash('alert-class', 'alert-success');
			return Redirect::to('user');
		} else {
			// permission error
			Session::flash('message', 'Немате одговарајуће пермисије да измените кориснички налог');
			Session::flash('alert-class', 'alert-danger');
			return Redirect::to('user');
		}
	}

	/**
	 * Displays login page
	 * GET /login
	 * 
	 * @return Response
	 */
    public function getLogin()
    {
    	if (Auth::check()) {
    		return Redirect::to('pretraga');
    	}
        return View::make('users.login');
    }

    /**
     * Process login of user
     * POST /login
     * 
     * @return Response
     */
    public function doLogin()
    {
    	// set validation data
    	$rules = array(
    		'email' => 'required|email',
    		'password' => 'required'
    		);

    	$attributeNames = array(
    		'email' => 'Е-пошта',
    		'password' => 'Шифра'
    		);
    	$validator = Validator::make(Input::all(), $rules);
    	$validator->setAttributeNames($attributeNames);

    	// process validation
    	if ($validator->fails()) {
    		return Redirect::to('login')
    			->withErrors($validator);
    	} else {
    		//set user data
    		$userdata = array(
    			'email' => Input::get('email'),
    			'password' => Input::get('password')
    			);
    		// check auth with provided data
    		if (Auth::attempt($userdata)) {
    			$sess_data = array(
    				'uid' => Auth::user()->id,
    				'full_name' => Auth::user()->full_name,
    				'email'	=> Auth::user()->email,
    				);
    			if (Auth::user()->sudo === 1) {
    				$sess_data['sudo'] = true;
    			}
    			Session::put($sess_data);
    			return Redirect::to('alkometri');
    		} else {
    			return Redirect::to('login');
    		}
    	}
    }

    /**
     * Process logout of user
     * GET /logout
     * 
     * @return Response
     */
    public function doLogout()
    {
    	Auth::logout();
    	Session::flush();
    	return Redirect::to('login');
    }
}