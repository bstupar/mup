<?php

class PupravaController extends \BaseController {

	protected $layout = 'main';

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	/**
	 * Index page of Puprava
	 * GET /uprava
	 * 
	 * @return Response
	 */
	public function index()
	{
		$data['uprave'] = Puprava::all();
		return View::make('puprava.index', $data);
	}

	/**
	 * Store function for new puprava
	 * POST /uprava
	 * 
	 * @return Redirect response
	 */
	public function store()
	{

		$rules = array(
			'ime_puprava' => 'required|unique:policijska_uprava,ime_puprava'
			);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('uprava')
							->withErrors($validator)
							->withInput();
		} else {
			$uprava = new Puprava();
			$uprava->ime_puprava = Input::get('ime_puprava');
			$uprava->save();

			Session::flash('message', 'Успешно додата Полицијска управа');
			Session::flash('alert-class', 'alert-success');
			return Redirect::to('uprava');
		}
	}

	/**
	 * Edit method puprava and add pstanica
	 * GET /uprava/{id}
	 * 
	 * @param  [int] $id [ID of puprava in DB]
	 * @return Response
	 */
	public function edit($id)
	{
		$data['uprava'] = Puprava::find($id);
		$data['stanice'] = Pstanica::where('id_puprava', $id)->get();
		return View::make('puprava.edit', $data);
	}

	/**
	 * Update method for puprava
	 * PUT /uprava/{id}
	 * 
	 * @param  [int] $id [ID to puprava in DB]
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'ime_puprava' => 'required|unique:policijska_uprava,ime_puprava'
			);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('uprava/' . $id)
							->withErrors($validator)
							->withInput();
		} else {
			$uprava = Puprava::find($id);
			$uprava->ime_puprava = Input::get('ime_puprava');
			$uprava->save();

			Session::flash('message', 'Успешно промењено име');
			Session::flash('alert-class', 'alert-success');
			return Redirect::to('uprava/' . $id);
		}
	}

	/**
	 * Store new PStanica
	 * POST /uprava/{id}
	 * 
	 * @param  [int] $id [ID of pstanica in DB]
	 * @return Response
	 */
	public function storeStanica($id)
	{
		$rules = array(
			'ime_pstanica' => 'required|unique:policijska_stanica,ime_pstanica'
			);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('uprava/' . $id)
						->withErrors($validator)
						->withInput();
		} else {
			$stanica = new Pstanica();
			$stanica->ime_pstanica = Input::get('ime_pstanica');
			$stanica->id_puprava = $id;
			$stanica->save();

			Session::flash('message', '');
			Session::flash('alert-class', 'alert-success');
			return Redirect::to('uprava/' . $id);
		}
	}
}