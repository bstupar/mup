<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => " :attribute се мора прихватити.",
	"active_url"           => " :attribute није исправан УРЛ.",
	"after"                => " :attribute мора бити датум после :date.",
	"alpha"                => " :attribute може да садржи само слова.",
	"alpha_dash"           => " :attribute може да садржи само слова, бројеве и цртице.",
	"alpha_num"            => " :attribute може да садржи само слова и бројеве.",
	"array"                => " :attribute мора бити низ.",
	"before"               => " :attribute мора бити датум пре :date.",
	"between"              => array(
		"numeric" => " :attribute мора бити између :min и :max.",
		"file"    => " :attribute мора бити између :min и :max килобајта.",
		"string"  => " :attribute мора бити између :min и :max карактера.",
		"array"   => " :attribute мора имати између :min и :max ставки.",
	),
	"boolean"              => " :attribute поље мора бити тачно или нетачно.",
	"confirmed"            => " :attribute потврда се не поклапа.",
	"date"                 => " :attribute није правилан датум.",
	"date_format"          => " :attribute не одговара формату :format.",
	"different"            => " :attribute и :other морају бити различити.",
	"digits"               => " :attribute мора бити :digits бројева.",
	"digits_between"       => " :attribute мора бити између :min и :max бројева.",
	"email"                => " :attribute мора бити a исправна адреса е-поште.",
	"exists"               => " изабрани :attribute није валидан.",
	"image"                => " :attribute мора бити слика.",
	"in"                   => " изабрани :attribute није валидан.",
	"integer"              => " :attribute мора бити цео број.",
	"ip"                   => " :attribute мора бити исправна ИП адреса.",
	"max"                  => array(
		"numeric" => " :attribute не сме бити веће од :max.",
		"file"    => " :attribute не сме бити веће од :max килобајта.",
		"string"  => " :attribute не сме бити веће од :max карактера.",
		"array"   => " :attribute не сме имати више од :max ставки.",
	),
	"mimes"                => " :attribute мора бити документ типа: :values.",
	"min"                  => array(
		"numeric" => " :attribute мора бити најмање :min.",
		"file"    => " :attribute мора бити најмање :min килобајта.",
		"string"  => " :attribute мора бити најмање :min карактера.",
		"array"   => " :attribute мора имати најмање :min ставки.",
	),
	"not_in"               => " Изабрани :attribute није валидан.",
	"numeric"              => " :attribute мора бити број.",
	"regex"                => " :attribute формат није исправан.",
	"required"             => " :attribute поље је подразумевано.",
	"required_if"          => " :attribute поље је подразумевано када :other је :value.",
	"required_with"        => " :attribute поље је подразумевано када је вредност :values .",
	"required_with_all"    => " :attribute поље је подразумевано када је вредност :values .",
	"required_without"     => " :attribute поље је подразумевано када вредност није :values .",
	"required_without_all" => " :attribute поље је подразумевано када ниједна вредност није :values .",
	"same"                 => " :attribute и :other се морају поклопити.",
	"size"                 => array(
		"numeric" => " :attribute мора бити :size.",
		"file"    => " :attribute мора бити :size килобајта.",
		"string"  => " :attribute мора бити :size карактера.",
		"array"   => " :attribute мора садржати :size ставки.",
	),
	"unique"               => " :attribute је заузет.",
	"url"                  => " :attribute формат није исправан.",
	"timezone"             => " :attribute мора бити валидна зона.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
