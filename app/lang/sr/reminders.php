<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Шифра мора бити најмање шест карактера и мора се поклопити са поновљеном шифром.",

	"user" => "Не може се пронаћи корисник са том адресом е-поште.",

	"token" => "Токен за ресетовање шифре није исправан.",

	"sent" => "Послат подсетник за шифру!",

	"reset" => "Шифра је ресетована!",

);
