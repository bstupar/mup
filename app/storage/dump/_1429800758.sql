-- MySQL dump 10.13  Distrib 5.6.19, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mup
-- ------------------------------------------------------
-- Server version	5.6.19-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alkometri`
--

DROP TABLE IF EXISTS `alkometri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alkometri` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alkoproizvodjac_id` int(10) unsigned NOT NULL,
  `alkomodel_id` int(10) unsigned NOT NULL,
  `serijski_broj` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `inventarski_broj` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `godina_proizvodnje` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `kalibracija` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `drzavni_zig` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `troskovi_kalibracije` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `troskovi_servisa` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `stanje` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('сервис','редован сервис','на коришћењу','за расход') COLLATE utf8_unicode_ci NOT NULL,
  `napomena` text COLLATE utf8_unicode_ci NOT NULL,
  `pstanica_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alkometri`
--

LOCK TABLES `alkometri` WRITE;
/*!40000 ALTER TABLE `alkometri` DISABLE KEYS */;
INSERT INTO `alkometri` VALUES (1,1,2,'123456','12345/15','2010','22-Април-15','23-Април-15','12.12','32.89',1,'сервис','Prvi alkometar ',1,0,'2015-04-22 14:29:32','2015-04-23 12:59:52'),(2,1,2,'123456','12345/15','2010','22-Април-15','23-Април-15','12.12','32.89',0,'','Prvi alkometar ',1,0,'2015-04-23 13:17:36','2015-04-23 13:17:36'),(3,1,1,'9888','5895/15','2014','20-Април-15','23-Април-15','8995.29','65499.59',0,'','na servisu, pokvarena pumpa\r\naasd',2,0,'2015-04-23 13:21:31','2015-04-23 13:21:31');
/*!40000 ALTER TABLE `alkometri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `it_oprema`
--

DROP TABLE IF EXISTS `it_oprema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `it_oprema` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vrsta_it_opreme_id` int(10) unsigned NOT NULL,
  `marka_opreme_id` int(10) unsigned NOT NULL,
  `tip_opreme_id` int(10) unsigned NOT NULL,
  `inventarski_broj` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `serijski_broj` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `godina_proizvodnje` date NOT NULL,
  `stanje` tinyint(1) NOT NULL DEFAULT '0',
  `napomena` text COLLATE utf8_unicode_ci NOT NULL,
  `policijska_stanica_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `it_oprema_vrsta_it_opreme_id_foreign` (`vrsta_it_opreme_id`),
  KEY `it_oprema_marka_opreme_id_foreign` (`marka_opreme_id`),
  KEY `it_oprema_tip_opreme_id_foreign` (`tip_opreme_id`),
  KEY `it_oprema_policijska_stanica_id_foreign` (`policijska_stanica_id`),
  KEY `it_oprema_user_id_foreign` (`user_id`),
  CONSTRAINT `it_oprema_marka_opreme_id_foreign` FOREIGN KEY (`marka_opreme_id`) REFERENCES `marka_opreme` (`id`),
  CONSTRAINT `it_oprema_policijska_stanica_id_foreign` FOREIGN KEY (`policijska_stanica_id`) REFERENCES `policijska_stanica` (`id`),
  CONSTRAINT `it_oprema_tip_opreme_id_foreign` FOREIGN KEY (`tip_opreme_id`) REFERENCES `tip_opreme` (`id`),
  CONSTRAINT `it_oprema_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `it_oprema_vrsta_it_opreme_id_foreign` FOREIGN KEY (`vrsta_it_opreme_id`) REFERENCES `vrsta_it_opreme` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `it_oprema`
--

LOCK TABLES `it_oprema` WRITE;
/*!40000 ALTER TABLE `it_oprema` DISABLE KEYS */;
/*!40000 ALTER TABLE `it_oprema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marka_alkometra`
--

DROP TABLE IF EXISTS `marka_alkometra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marka_alkometra` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime_alkometra` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marka_alkometra`
--

LOCK TABLES `marka_alkometra` WRITE;
/*!40000 ALTER TABLE `marka_alkometra` DISABLE KEYS */;
INSERT INTO `marka_alkometra` VALUES (1,'AlcoQuant'),(2,'Dreger');
/*!40000 ALTER TABLE `marka_alkometra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marka_opreme`
--

DROP TABLE IF EXISTS `marka_opreme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marka_opreme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime_opreme` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marka_opreme`
--

LOCK TABLES `marka_opreme` WRITE;
/*!40000 ALTER TABLE `marka_opreme` DISABLE KEYS */;
/*!40000 ALTER TABLE `marka_opreme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marka_radara`
--

DROP TABLE IF EXISTS `marka_radara`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marka_radara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime_radara` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marka_radara`
--

LOCK TABLES `marka_radara` WRITE;
/*!40000 ALTER TABLE `marka_radara` DISABLE KEYS */;
/*!40000 ALTER TABLE `marka_radara` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marka_vozila`
--

DROP TABLE IF EXISTS `marka_vozila`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marka_vozila` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime_vozila` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marka_vozila`
--

LOCK TABLES `marka_vozila` WRITE;
/*!40000 ALTER TABLE `marka_vozila` DISABLE KEYS */;
/*!40000 ALTER TABLE `marka_vozila` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2015_02_01_212952_create_user_table',1),('2015_02_01_222006_create_policijska_uprava',1),('2015_02_01_222133_create_policijska_stanica',1),('2015_02_02_203641_create_radarai',1),('2015_02_02_204701_create_marka_radara',1),('2015_02_02_204850_create_tip_radara',1),('2015_02_02_205138_create_vozila',1),('2015_02_02_211412_create_zaduzenje_vozila',1),('2015_02_02_214330_create_vrsta_vozila',1),('2015_02_02_214435_create_obelezija_vozila',1),('2015_02_02_214627_create_tip_vozila',1),('2015_02_02_214734_create_marka_vozila',1),('2015_02_04_113305_create_it_oprema',1),('2015_02_04_114015_create_vrsta_it_opreme',1),('2015_02_04_114126_create_marka_opreme',1),('2015_02_04_114207_create_tip_opreme',1),('2015_02_04_133255_create_alkometri',1),('2015_02_04_134407_create_marka_alkometra',1),('2015_02_04_134511_create_tip_alkometra',1),('2015_02_04_135754_create_foreign_policijska_stanica',1),('2015_02_04_140247_create_foreign_vozila',1),('2015_02_04_141025_create_foreign_radari',1),('2015_02_04_141057_create_foreign_it_oprema',1),('2015_02_04_141130_create_foreign_alkometri',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_alkometra`
--

DROP TABLE IF EXISTS `model_alkometra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_alkometra` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_alkometra` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_alkometra`
--

LOCK TABLES `model_alkometra` WRITE;
/*!40000 ALTER TABLE `model_alkometra` DISABLE KEYS */;
INSERT INTO `model_alkometra` VALUES (1,'6020',1),(2,'6020 plus',1);
/*!40000 ALTER TABLE `model_alkometra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obelezija_vozila`
--

DROP TABLE IF EXISTS `obelezija_vozila`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obelezija_vozila` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `obelezija` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obelezija_vozila`
--

LOCK TABLES `obelezija_vozila` WRITE;
/*!40000 ALTER TABLE `obelezija_vozila` DISABLE KEYS */;
/*!40000 ALTER TABLE `obelezija_vozila` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policijska_stanica`
--

DROP TABLE IF EXISTS `policijska_stanica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policijska_stanica` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime_pstanica` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_puprava` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `policijska_stanica_policijska_uprava_id_foreign` (`id_puprava`),
  CONSTRAINT `policijska_stanica_policijska_uprava_id_foreign` FOREIGN KEY (`id_puprava`) REFERENCES `policijska_uprava` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policijska_stanica`
--

LOCK TABLES `policijska_stanica` WRITE;
/*!40000 ALTER TABLE `policijska_stanica` DISABLE KEYS */;
INSERT INTO `policijska_stanica` VALUES (1,'Нови Београд',1),(2,'Земун',1),(3,'Палилула',1),(4,'Дорћол',1);
/*!40000 ALTER TABLE `policijska_stanica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policijska_uprava`
--

DROP TABLE IF EXISTS `policijska_uprava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policijska_uprava` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime_puprava` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policijska_uprava`
--

LOCK TABLES `policijska_uprava` WRITE;
/*!40000 ALTER TABLE `policijska_uprava` DISABLE KEYS */;
INSERT INTO `policijska_uprava` VALUES (1,'Управа Београдa'),(2,'Управа Зрењанин'),(3,'Управа Нови Сад');
/*!40000 ALTER TABLE `policijska_uprava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `radari`
--

DROP TABLE IF EXISTS `radari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `radari` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marka_radara_id` int(10) unsigned NOT NULL,
  `tip_radara_id` int(10) unsigned NOT NULL,
  `serijski_broj` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `inventarski_broj` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `godina_proizvodnje` date NOT NULL,
  `kalibracija` date NOT NULL,
  `drzavni_zig` date NOT NULL,
  `troskovi_kalibracije` decimal(14,2) NOT NULL,
  `troskovi_servisa` decimal(14,2) NOT NULL,
  `stanje` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('сервис','редован сервис','на коришћењу','за расход') COLLATE utf8_unicode_ci NOT NULL,
  `napomena` text COLLATE utf8_unicode_ci NOT NULL,
  `policijska_stanica_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `radari_marka_radara_id_foreign` (`marka_radara_id`),
  KEY `radari_tip_radara_id_foreign` (`tip_radara_id`),
  KEY `radari_policijska_stanica_id_foreign` (`policijska_stanica_id`),
  KEY `radari_user_id_foreign` (`user_id`),
  CONSTRAINT `radari_marka_radara_id_foreign` FOREIGN KEY (`marka_radara_id`) REFERENCES `marka_radara` (`id`),
  CONSTRAINT `radari_policijska_stanica_id_foreign` FOREIGN KEY (`policijska_stanica_id`) REFERENCES `policijska_stanica` (`id`),
  CONSTRAINT `radari_tip_radara_id_foreign` FOREIGN KEY (`tip_radara_id`) REFERENCES `tip_radara` (`id`),
  CONSTRAINT `radari_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `radari`
--

LOCK TABLES `radari` WRITE;
/*!40000 ALTER TABLE `radari` DISABLE KEYS */;
/*!40000 ALTER TABLE `radari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_opreme`
--

DROP TABLE IF EXISTS `tip_opreme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tip_opreme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_opreme`
--

LOCK TABLES `tip_opreme` WRITE;
/*!40000 ALTER TABLE `tip_opreme` DISABLE KEYS */;
/*!40000 ALTER TABLE `tip_opreme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_radara`
--

DROP TABLE IF EXISTS `tip_radara`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tip_radara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_radara`
--

LOCK TABLES `tip_radara` WRITE;
/*!40000 ALTER TABLE `tip_radara` DISABLE KEYS */;
/*!40000 ALTER TABLE `tip_radara` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_vozila`
--

DROP TABLE IF EXISTS `tip_vozila`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tip_vozila` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tip` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_vozila`
--

LOCK TABLES `tip_vozila` WRITE;
/*!40000 ALTER TABLE `tip_vozila` DISABLE KEYS */;
/*!40000 ALTER TABLE `tip_vozila` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sudo` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policijska_stanica_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Бошко Ступар','$2y$10$EITMY6u8tK/BH19soN0X8uyTUvzlmzF7toZjWTx/emPsf6V1UaTWy','bosko@sky-tec.rs',1,'2015-02-25 15:10:07','2015-03-02 11:03:21','8wlBvHZKGIlorWGDw8C3TeCbdPIWorpAiFxzezEGgYt2WVayI8W6Hu0lWOPL',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vozila`
--

DROP TABLE IF EXISTS `vozila`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vozila` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inventarski_broj` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `registarski_broj` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `marka_vozila_id` int(10) unsigned NOT NULL,
  `tip_vozila_id` int(10) unsigned NOT NULL,
  `godina_proizvodnje` date NOT NULL,
  `sasija` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `vrsta_vozila_id` int(10) unsigned NOT NULL,
  `obelezija_vozila_id` int(10) unsigned NOT NULL,
  `pocetna_km` decimal(10,2) NOT NULL,
  `krajnja_km` decimal(10,2) NOT NULL,
  `stanje` tinyint(1) NOT NULL DEFAULT '0',
  `zaduzenje_vozila_id` int(10) unsigned NOT NULL,
  `status` enum('сервис','редован сервис','на коришћењу','за расход') COLLATE utf8_unicode_ci NOT NULL,
  `napomena` text COLLATE utf8_unicode_ci NOT NULL,
  `policijska_stanica_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `vozila_marka_vozila_id_foreign` (`marka_vozila_id`),
  KEY `vozila_tip_vozila_id_foreign` (`tip_vozila_id`),
  KEY `vozila_vrsta_vozila_id_foreign` (`vrsta_vozila_id`),
  KEY `vozila_obelezija_vozila_id_foreign` (`obelezija_vozila_id`),
  KEY `vozila_zaduzenje_vozila_id_foreign` (`zaduzenje_vozila_id`),
  KEY `vozila_policijska_stanica_id_foreign` (`policijska_stanica_id`),
  KEY `vozila_user_id_foreign` (`user_id`),
  CONSTRAINT `vozila_marka_vozila_id_foreign` FOREIGN KEY (`marka_vozila_id`) REFERENCES `marka_vozila` (`id`),
  CONSTRAINT `vozila_obelezija_vozila_id_foreign` FOREIGN KEY (`obelezija_vozila_id`) REFERENCES `obelezija_vozila` (`id`),
  CONSTRAINT `vozila_policijska_stanica_id_foreign` FOREIGN KEY (`policijska_stanica_id`) REFERENCES `policijska_stanica` (`id`),
  CONSTRAINT `vozila_tip_vozila_id_foreign` FOREIGN KEY (`tip_vozila_id`) REFERENCES `tip_vozila` (`id`),
  CONSTRAINT `vozila_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `vozila_vrsta_vozila_id_foreign` FOREIGN KEY (`vrsta_vozila_id`) REFERENCES `vrsta_vozila` (`id`),
  CONSTRAINT `vozila_zaduzenje_vozila_id_foreign` FOREIGN KEY (`zaduzenje_vozila_id`) REFERENCES `zaduzenje_vozila` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vozila`
--

LOCK TABLES `vozila` WRITE;
/*!40000 ALTER TABLE `vozila` DISABLE KEYS */;
/*!40000 ALTER TABLE `vozila` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrsta_it_opreme`
--

DROP TABLE IF EXISTS `vrsta_it_opreme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrsta_it_opreme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vrsta_opreme` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrsta_it_opreme`
--

LOCK TABLES `vrsta_it_opreme` WRITE;
/*!40000 ALTER TABLE `vrsta_it_opreme` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrsta_it_opreme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrsta_vozila`
--

DROP TABLE IF EXISTS `vrsta_vozila`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrsta_vozila` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vrsta_vozila` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrsta_vozila`
--

LOCK TABLES `vrsta_vozila` WRITE;
/*!40000 ALTER TABLE `vrsta_vozila` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrsta_vozila` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zaduzenje_vozila`
--

DROP TABLE IF EXISTS `zaduzenje_vozila`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zaduzenje_vozila` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime_vozaca` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `policijska_stanica_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zaduzenje_vozila`
--

LOCK TABLES `zaduzenje_vozila` WRITE;
/*!40000 ALTER TABLE `zaduzenje_vozila` DISABLE KEYS */;
/*!40000 ALTER TABLE `zaduzenje_vozila` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-23 14:52:38
