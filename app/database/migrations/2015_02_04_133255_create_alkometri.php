<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlkometri extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alkometri', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('marka_alkometra_id')->unsigned();
            $table->integer('tip_alkometra_id')->unsigned();
            $table->string('serijski_broj', 45);
            $table->string('inventarski_broj', 45);
            $table->date('godina_proizvodnje');
            $table->date('kalibracija');
            $table->date('drzavni_zig');
            $table->decimal('troskovi_kalibracije', 14, 2);
            $table->decimal('troskovi_servisa', 14, 2);
            $table->boolean('stanje')->default(0);
            $table->enum('status', array('сервис', 'редован сервис', 'на коришћењу', 'за расход'));
            $table->text('napomena');
            $table->integer('policijska_stanica_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alkometri');
	}

}
