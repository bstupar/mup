<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZaduzenjeVozila extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('zaduzenje_vozila', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('ime_vozaca', 120);
            $table->integer('policijska_stanica_id')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('zaduzenje_vozila');
	}

}
