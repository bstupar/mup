<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVozila extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vozila', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('inventarski_broj', 45);
            $table->string('registarski_broj', 60);
            $table->integer('marka_vozila_id')->unsigned();
            $table->integer('tip_vozila_id')->unsigned();
            $table->date('godina_proizvodnje');
            $table->string('sasija', 45);
            $table->integer('vrsta_vozila_id')->unsigned();
            $table->integer('obelezija_vozila_id')->unsigned();
            $table->decimal('pocetna_km', 10, 2);
            $table->decimal('krajnja_km', 10, 2);
            $table->boolean('stanje')->default(0);
            $table->integer('zaduzenje_vozila_id')->unsigned();
            $table->enum('status', array('сервис', 'редован сервис', 'на коришћењу', 'за расход'));
            $table->text('napomena');
            $table->integer('policijska_stanica_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vozila');
	}

}
