<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItOprema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('it_oprema', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('vrsta_it_opreme_id')->unsigned();
            $table->integer('marka_opreme_id')->unsigned();
            $table->integer('tip_opreme_id')->unsigned();
            $table->string('inventarski_broj', 45);
            $table->string('serijski_broj', 45);
            $table->date('godina_proizvodnje');
            $table->boolean('stanje')->default(0);
            $table->text('napomena');
            $table->integer('policijska_stanica_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('it_oprema');
	}

}
