<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicijskaStanica extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('policijska_stanica', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('ps_ime');
            $table->integer('policijska_uprava_id')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('policijska_stanica');
	}

}
