<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content>
    <meta name="author" content="Boško Stupar - Sky Technologies">
    <link rel="icon" href="{{ URL::to('/') }}/favicon.ico">
    <title>МУП Евиденција</title>

    <!-- CSS -->
    {{ HTML::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
    {{ HTML::style('assets/css/bootstrap-theme.min.css') }}
    {{ HTML::style('assets/css/dataTables.bootstrap.css') }}
    {{ HTML::style('assets/css/dataTables.responsive.css') }}
    {{ HTML::style('assets/css/bootstrap-datepicker3.min.css') }}
    {{ HTML::style('assets/css/mup.css') }}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- JavaScript -->
    {{ HTML::script('assets/bower_components/jquery/dist/jquery.min.js') }}
    {{ HTML::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}
    {{ HTML::script('assets/js/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/js/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/js/jquery.numeric.js') }}
    {{ HTML::script('assets/js/bootstrap-datepicker.js') }}
    {{ HTML::script('assets/js/locales/bootstrap-datepicker.sr.min.js')}}
    {{ HTML::script('assets/js/excellentexport.min.js') }}
    {{ HTML::script('assets/js/dt.js') }}
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">Муп.апп</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li @if (Request::path() == 'alkometri') class="active" @endif>
                    <a href="{{ URL::to('alkometri') }}">Алкометри</a>
                </li>
                <li @if (Request::path() == 'uprava') class="active" @endif>
                    <a href="{{ URL::to('uprava') }}">Полицијска Управа</a>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                       {{Auth::user()->full_name}} <span class="glyphicon glyphicon glyphicon-user"></span>  <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ URL::to('user/'. Session::get('uid')) }}"><span class="glyphicon glyphicon-user"></span> Кориснички профил</a>
                        </li>
                        <li class="divider"></li>
                        @if (Auth::user()->sudo === 1) 
                        <li><a href="{{ URL::to('user') }}">Листа корисника</a></li>
                        <li><a href="{{ URL::to('user/create') }}">Додавање корисника</a></li>
                        <li class="divider"></li>
                        @endif
                        <li><a href="{{ URL::to('logout') }}"><span class="glyphicon glyphicon-log-out"></span> Излогуј се</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>
            
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<div class="container-fluid">

 @yield('content')
</div>
<footer class="footer">
    <div class="container">
        <p class="text-muted pull-left">&copy;2015 SKY Technologies </p>
        <p class="text-muted pull-right"><span class="glyphicon glyphicon-time"></span> {{ number_format((microtime(true) - LARAVEL_START), 4) }} секунди</p>
    </div>
</footer>
</body>
</html>