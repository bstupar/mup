@extends('main')

@section('content')
    <!-- Table results -->
    <div class="row">
        <div class="col-xs-12">
         <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li class="active">Полицијска управа</li>
        </ol> 
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endif

        @foreach( $errors->all() as $val)
            <p class="alert alert-warning">{{ $val }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endforeach  
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            
            <div class="panel panel-default">
                <div class="panel-heading">Додати Полицијску управу</div>
                <div class="panel-body">
                    {{ Form::open() }}

                    <fieldset>
                        <div class="form-group">
                            {{ Form::text('ime_puprava', Input::old('ime_puprava'), array('class' => 'form-control', 'placeholder' => 'Полицијска управа', 'autofocus')) }}
                        </div>
                        {{ Form::submit('Додај', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <table class="table">
                <tr>
                    
                    <th>Листа полицијских управа</th>
                    <th></th>
                </tr>
                @foreach ($uprave as $uprava)
                    <tr>
                        <td><a href="{{ URL::to('uprava/' . $uprava->id )}}">{{ $uprava->ime_puprava }}</a></td>
                        <td><a class="btn-modal" title="Изменити" href="{{ URL::to('uprava/' . $uprava->id )}}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a></td>
                    </tr>
                @endforeach
            </table>
            
        </div>
    </div>
@stop