@extends('main')

@section('content')
    <!-- Table results -->
    <div class="row">
        <div class="col-xs-12">
         <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('uprava') }}">Полицијска Управа</a></li>
            <li class="active">{{ $uprava->ime_puprava }}</li>
        </ol> 

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endif

        @foreach( $errors->all() as $val)
            <p class="alert alert-warning">{{ $val }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endforeach  
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-heading">Изменити име Полицијске управе</div>
                <div class="panel-body">
                    {{ Form::open(array('action' => array('PupravaController@update', $uprava->id), 'method' => 'put')) }}
                    
                    <fieldset>
                        <div class="form-group">
                            <input type="text" name="ime_puprava" class="form-control" value="{{ $uprava->ime_puprava }}">
                        </div>
                        {{ Form::submit('Измени', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-heading">Додати Полицијску Станицу</div>
                <div class="panel-body">
                    {{ Form::open() }}
                    <fieldset>
                        <div class="form-group">
                            {{ Form::text('ime_pstanica', Input::old('ime_pstanica'), array('class' => 'form-control', 'placeholder' => 'Полицијска Станица', 'autofocus')) }}
                        </div>
                        {{ Form::submit('Додај', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <table class="table">
                <tr>
                    <th>Полицијске Станице</th>
                </tr>
                @foreach ($stanice as $stanica)
                    <tr>
                        <td>{{ $stanica->ime_pstanica }} </td>
                    </tr>
                @endforeach
            </table>
            
        </div>
    </div>
@stop