@extends('main')

@section('content')
<div class="row">
	<div class="col-xs-5">
		<h1 class="page-header">Кориснички профил</h1>
	</div>
</div>
<div class="row">
	<div class="col-xs-5">
		<div class="panel panel-default">
			<div class="panel-heading">
				Подаци корисника
			</div>
			<div class="panel-body">
				
						<dl class="dl-horizontal">
							<dt>Корисник</dt><dd>{{ $user->full_name }}</dd>
							<dt>E-пошта</dt><dd>{{ $user->email }}</dd>
						</dl>
						<a href="{{ URL::to('user/'. $user->id .'/edit') }}" class="btn btn-primary btn-sm">Промена шифре</a>
					
			</div>
		</div>
	</div>
</div>
@stop