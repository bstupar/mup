@extends('main')

@section('content')
<div class="row">
	<div class="col-xs-12">
		<h1 class="page-header">Кориснички профил</h1>
	</div>
</div>
<div class="row">
	<div class="col-xs-5">
		<div class="panel panel-default">
			<div class="panel-heading">
				Подаци корисника
			</div>
			<div class="panel-body">	
				<dl class="dl-horizontal">
					<dt>Корисник</dt><dd>{{ $user->full_name }}</dd>
					<dt>E-пошта</dt><dd>{{ $user->email }}</dd>
				</dl>
				<hr />
				<h4>Промена шифре</h4>
				@if (Session::has('message'))
					<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
					</p>
				@endif
				{{ Form::open(array('url' => 'user/'. $user->id .'/changePass' , 'method' => 'post' )) }}
				<fieldset>
					@if (Auth::user()->id === $user->id)
					<div class="form-group">
						{{ Form::password('old_password', array('class' => 'form-control', 'placeholder' => 'Стара шифра')) }}
					</div>
					@endif
					<div class="form-group">
						{{ Form::password('new_password', array('class' => 'form-control', 'placeholder' => 'Нова шифра')) }}
					</div>
					<div class="form-group">
						{{ Form::password('new_pass_repeat', array('class' => 'form-control', 'placeholder' => 'Поновите нову шифру')) }}
					</div>

					{{ Form::submit('Промени', array('class' => 'btn btn-lg btn-success btn-block')) }}
				</fieldset>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop