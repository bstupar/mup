@extends('main')

@section('content')
<div class="row">
	<div class="col-xs-6">
		<h1 class="page-header">Кориснички профил</h1>
	</div>
</div>
<div class="row">
	<div class="col-xs-6">
		@if(Session::has('message'))
			<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
			</p>
		@endif
		<div class="panel panel-default">
			<div class="panel-heading">
				Листа корисника 
				<a href="{{ URL::to('user/create') }}" class="btn  btn-xs btn-success pull-right" role='button'>Додај корисника</a>
			</div>
			<div class="panel-body">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<td>Име</td>
							<td>Е-пошта</td>
							<td>Опције</td>
						</tr>
					</thead>
					<tbody>
					@foreach ($users as $user => $value)
						<tr>
							<td>{{ $value->full_name }}</td>
							<td>{{ $value->email }}</td>
							<td>
								<a href="{{ URL::to('user/'. $value->id .'/edit') }}" class="btn btn-xs btn-info pull-left" style="margin-right:5px">Изменити</a>
								@if (Auth::user()->id !== $value->id)	
								{{ Form::open(array('url' => URL::to('user/'. $value->id), 'method' => 'delete', 'class' => 'pull-left'))}}
								{{ Form::submit('Обриши', array('class' => 'btn btn-xs btn-danger')) }}
								{{ Form::close() }}
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop