<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=windows-1251">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        @section('title')
           Evidencija
        @show
    </title>

    <!-- CSS -->
    {{ HTML::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
    {{ HTML::style('assets/bower_components/metisMenu/dist/metisMenu.min.css') }}
    {{ HTML::style('assets/css/sb-admin-2.css') }}
    {{ HTML::style('assets/bower_components/font-awesome/css/font-awesome.min.css') }}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    {{ HTML::script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}
    <![endif]-->

</head>
<body>
@yield('content')

<!-- JavaScript -->
{{ HTML::script('assets/bower_components/jquery/dist/jquery.min.js') }}
{{ HTML::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}
{{ HTML::script('assets/bower_components/metisMenu/dist/metisMenu.min.js') }}
{{ HTML::script('assets/js/sb-admin-2.js') }}

</body>

</html>