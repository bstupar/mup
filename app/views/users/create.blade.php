@extends('main')

@section('content')
<div class="row">
    <div class="col-xs-5">
        <h1 class="page-header">Додавање корисника</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-5">

    {{ HTML::ul($errors->all(), array('class' => 'list-group')) }}
    
        
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                {{ Form::open( array('url' => 'user')) }}
                
                    <fieldset>
                        <div class="form-group">

                            {{ Form::text('full_name', Input::old('full_name'), array('class' => 'form-control', 'placeholder' => 'Име и презиме', 'autofocus')) }}
                           
                        </div>
                        <div class="form-group">

                            {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'E-пошта')) }}
                            
                        </div>
                        <div class="form-group">

                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Шифра')) }}
                            
                            
                        </div>
                        <div class="form-group">

                            {{ Form::password('re_password', array('class' => 'form-control', 'placeholder' => 'Поновити шифру')) }}
                            
                        </div>

                        {{ Form::submit('Региструј', array('class' => 'btn btn-lg btn-success btn-block')) }}
                        
                    </fieldset>
                {{ Form::close() }}
            </div>
        
    </div>
</div>
@stop