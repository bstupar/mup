@extends('main')

@section('content')
    <!-- Table results -->
    <div class="row">
        <div class="col-xs-12">
         <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('alkometri') }}">Алкометри</a></li>
            <li class="active">Произвођачи</li>
        </ol> 
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endif

        @foreach( $errors->all() as $val)
            <p class="alert alert-warning">{{ $val }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endforeach  
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            
            <div class="panel panel-default">
                <div class="panel-heading">Додати произвођача</div>
                <div class="panel-body">
                    {{ Form::open() }}

                    <fieldset>
                        <div class="form-group">
                            {{ Form::text('ime_alkometra', Input::old('ime_alkometra'), array('class' => 'form-control', 'placeholder' => 'Име произвођача', 'autofocus')) }}
                        </div>
                        {{ Form::submit('Додај', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <table class="table">
                <tr>
                    
                    <th>Листа произвођача</th>
                    <th></th>
                </tr>
                @foreach ($proizvodjaci as $marka)
                    <tr>
                        <td><a href="{{ URL::to('alkometri/proizvodjaci/' . $marka->id )}}">{{ $marka->ime_alkometra }}</a></td>
                        <td><a class="btn-modal" title="Изменити" href="{{ URL::to('alkometri/proizvodjaci/' . $marka->id )}}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a></td>
                    </tr>
                @endforeach
            </table>
            
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="proizvodjac" tabindex="-1" role="dialog" aria-labelledby="proizvodjacLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="proizvodjacLabel">Izmena proizvodjaca</h4>
          </div>
          <div class="modal-body">
            {{ Form::open() }}
                
            {{ Form::close() }}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
@stop