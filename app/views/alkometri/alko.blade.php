@extends('main')

@section('content')
<!-- Table results -->
<div class="row">
    <div class="col-xs-12">

        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li class='active'>Alkometri</li>
        </ol>
        <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                Додај <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL::to('alkometri/dodaj') }}">Алкометар</a></li>
                <li><a href="{{ URL::to('alkometri/proizvodjaci') }}">Произвођач / Модел</a></li>
            </ul>
        </div>
        <a class="btn btn-primary " download="alko.xls" href="#" onclick="return ExcellentExport.excel(this, 'alkometri', 'Sheet Name Here');">Извези у Ексел</a>
        <hr/>
    <div class="form-inline">
        <table id="alkometri" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Бр.</th>
                    <th>Произвођач</th>
                    <th>Модел</th>
                    <th>Серијски бр.</th>
                    <th>Инвентарски бр.</th>
                    <th>Произведен</th>
                    <th>Калибрација</th>
                    <th>Државни жиг</th>
                    <th>Трошкови калибрације</th>
                    <th>Tрошкови сервиса</th>
                    <th>Стање</th>
                    <th>Статус</th>
                    <th>Напомена</th>
                    <th>Полицијска Станица</th>
                </tr>
            </thead>
            
        </table>
        <p>

        </p>
    </div>
    </div>
</div>
<script>

    $('#alkometri').dataTable( {
            'language': {
                'url': '{{ URL::to('/') }}/assets/js/dataTables.srpski.json'
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{{ URL::to('/') }}/test",
            "columns": [
                { "data": "id"},
                { "data": "ime_alkometra"},
                { "data": "model"},
                { "data": "serijski_broj"},
                { "data": "inventarski_broj"},
                { "data": "godina_proizvodnje"},
                { "data": "kalibracija"},
                { "data": "drzavni_zig"},
                { "data": "troskovi_kalibracije"},
                { "data": "troskovi_servisa"},
                { "data": "stanje"},
                { "data": "status"},
                { "data": "napomena"},
                { "data": "ime_pstanica"}
            ],

        });
</script>
@stop