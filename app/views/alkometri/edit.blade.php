@extends('main')

@section('content')
    <!-- Table results -->
    <div class="row">
        <div class="col-xs-12">
         <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('alkometri') }}">Алкометри</a></li>
            <li class="active">Измена алкометра</li>
        </ol> 

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endif

        @foreach( $errors->all() as $val)
            <p class="alert alert-warning">{{ $val }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endforeach  
        </div>
    </div>
    <!-- Form Start -->
    {{ Form::open(array('action' => array('AlkometriController@update', $alkometar->id), 'method' => 'put')) }}
    <div class="row">
        <!-- col 1 -->
        <div class="col-xs-3">
            <div class="form-group">
                <label for="proizvodjac">Произвођач алкометра</label>
                <select name="proizvodjac" id="proizvodjac" class="form-control" >
                    <option>Изабрати Произвођача</option>
                    @foreach ($proizvodjaci as $proizvodjac)
                            <option value="{{ $proizvodjac['id'] }}" @if ($alkometar->alkoproizvodjac_id == $proizvodjac['id']) selected @endif>{{ $proizvodjac['ime_alkometra'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="godina_proizvodnje">Година производње</label>
                <div class="input-group date" id="godina_proizvodnje">
                    {{ Form::text('godina_proizvodnje', Input::old('godina_proizvodnje', $alkometar->godina_proizvodnje), array('class' => 'form-control', 'placeholder' => 'Година производње')) }}
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label for="stanje">Стање</label>
                <select name="stanje" id="stanje" class="form-control">
                    <option value="0" @if ($alkometar->stanje == 0) selected @endif>Није на стању</option>
                    <option value="1" @if ($alkometar->stanje == 1) selected @endif>На стању</option>
                </select>
            </div>
        </div>
        <!-- Col 2 -->
        <div class="col-xs-3">
            <div class="form-group">
                <label for="model">Модел алкометра</label>
                <select name="model" id="model" class="form-control">
                    <option>Изабрати модел</option>
                </select>
            </div>
            <div class="form-group">
                <label for="kalibracija">Датум калибрације</label>
                <div class="input-group date" id="kalibracija">
                    {{ Form::text('kalibracija', Input::old('kalibracija', $alkometar->kalibracija), array('class' => 'form-control', 'placeholder' => 'Датум калибрације')) }}
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label for="status">Статус алкометра</label>
                <select name="status" id="status" class="form-control">
                    <option value="Сервис" @if ($alkometar->status === 'Сервис')  selected @endif >Сервис</option>
                    <option value="Редован сервис" @if ($alkometar->status === 'Редован сервис')  selected @endif >Редован сервис</option>
                    <option value="На коришћењу" @if ($alkometar->status === 'На коришћењу')  selected @endif >На коришћењу</option>
                    <option value="За расход" @if ($alkometar->status === 'За расход')  selected @endif >За расход</option>
                </select>
            </div>        
        </div>
        <!-- col 3 -->
        <div class="col-xs-3">
            <div class="form-group">
                <label for="serijski_broj">Серијски број</label>
                {{ Form::text('serijski_broj', Input::old('serijski_broj', $alkometar->serijski_broj), array('class' => 'form-control', 'placeholder' => 'Серијски Број')) }}
            </div>
            <div class="form-group">
                <label for="drzavni_zig">Датум државног жига</label>
                <div class="input-group date" id="drzavni_zig">
                    {{ Form::text('drzavni_zig', Input::old('drzavni_zig', $alkometar->drzavni_zig), array('class' => 'form-control', 'placeholder' => 'Датум државног жига')) }}
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label for="policijska_stanica_id">Полицијска Станица</label>
                <select name="policijska_stanica_id" id="policijska_stanica_id" class="form-control">
                    <option>Izabrati policijsku stanicu</option>
                    @foreach ($pstanice as $pstanica) 
                        <option value="{{ $pstanica['id'] }}" @if ($alkometar->pstanica_id == $pstanica['id']) selected @endif>{{ $pstanica['ime_pstanica'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!-- col 4 -->
        <div class="col-xs-3">
            <div class="form-group">
                <label for="inventarski_broj">Инвентарски број</label>
                {{ Form::text('inventarski_broj', Input::old('inventarski_broj', $alkometar->inventarski_broj), array('class' => 'form-control', 'placeholder' => 'Инвентарски Број')) }}
            </div>         
            <div class="form-group">
                <label for="troskovi_kalibracije">Трошкови калибрације</label>
                <div class="input-group">

                    {{ Form::text('troskovi_kalibracije', Input::old('troskovi_kalibracije', $alkometar->troskovi_kalibracije), array('class' => 'form-control', 'placeholder' => 'Трошкови Калибрације')) }}
                    <div class="input-group-addon">Дин.</div>
                </div>
            </div>
            <div class="form-group">
                <label for="troskovi_servisa">Трошкови сервиса</label>
                <div class="input-group">

                    {{ Form::text('troskovi_servisa', Input::old('troskovi_servisa', $alkometar->troskovi_servisa), array('class' => 'form-control', 'placeholder' => 'Трошкови Сервиса')) }}
                    <div class="input-group-addon">Дин.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="napomena">Напомена</label>
                {{ Form::textarea('napomena', Input::old('napomena', $alkometar->napomena), array('class' => 'form-control', 'placeholder' => 'Напомена', 'cols' => '30', 'rows' => '5')) }}
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <div class="form-group">
                        <div class="center-block">
                            {{ Form::submit('Изменити', array('class' => 'btn btn-lg btn-success btn-block')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <!-- Modal ajax loader -->
    <div class="modal fade bs-example-modal-sm" id="ajaxModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <h3 class="text-center">{{ HTML::image('assets/img/spinner.gif') }} Учитавање</h3> 
                </div>
            </div>
        </div>
    </div>
    <script>
    $(window).load(function() {
        function getModels( id )
        {
            $.ajax({
                type: 'POST',
                url: 'ajax',
                dataType: 'json',
                data: {id_alkometra: $('#proizvodjac').val(), token: $('input[name=_token]').val()},
                beforeSend: function() {
                    $('#ajaxModal').modal('show');
                },
                complete: function() {
                    $('#ajaxModal').modal('hide');
                },
                success: function(data) {   
                    // we need some default option, so ..           
                    var output = '<option>Izabrati model</option>';
                    //generate new option list with fetched data
                    for (var i = 0, len = data.length; i < len; ++i) {
                        if (data[i].id == id) {
                            output += '<option value="' + data[i].id + '"selected>' + data[i].model + '</option>';
                        } else {
                            output += '<option value="' + data[i].id + '">' + data[i].model + '</option>';
                        }
                    }
                    //clear old select options and append new
                    $('#model').empty();
                    $('#model').append(output);
                }
            });
        }
        getModels( {{ $alkometar->alkomodel_id }});
    })
    </script>
@stop