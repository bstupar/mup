@extends('main')

@section('content')
    <!-- Table results -->
    <div class="row">
        <div class="col-xs-12">
         <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('alkometri') }}">Алкометри</a></li>
            <li class="active">Додавање алкометра</li>
        </ol> 

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endif

        @foreach( $errors->all() as $val)
            <p class="alert alert-warning">{{ $val }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endforeach  
        </div>
    </div>
    <!-- Form Start -->
    {{ Form::open() }}
    <div class="row">
        <!-- col 1 -->
        <div class="col-xs-3">
            <div class="form-group">
                <select name="proizvodjac" id="proizvodjac" class="form-control" >
                    <option>Изабрати Произвођача</option>
                    @foreach($proizvodjaci as $proizvodjac)
                        <option value="{{ $proizvodjac['id'] }}">{{ $proizvodjac['ime_alkometra'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <div class="input-group date" id="godina_proizvodnje">
                    {{ Form::text('godina_proizvodnje', Input::old('godina_proizvodnje'), array('class' => 'form-control', 'placeholder' => 'Година производње')) }}
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </div>
            <div class="form-group">
                <select name="stanje" id="stanje" class="form-control">
                    <option value="0">Није на стању</option>
                    <option value="1">На стању</option>
                </select>
            </div>
        </div>
        <!-- Col 2 -->
        <div class="col-xs-3">
            <div class="form-group">
                <select name="model" id="model" class="form-control">
                    <option>Изабрати модел</option>
                </select>
            </div>
            <div class="form-group">
                <div class="input-group date" id="kalibracija">
                    {{ Form::text('kalibracija', Input::old('kalibracija'), array('class' => 'form-control', 'placeholder' => 'Датум калибрације')) }}
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </div>
            <div class="form-group">
                <select name="status" id="status" class="form-control">
                    <option value="Сервис">Сервис</option>
                    <option value="Редован сервис">Редован сервис</option>
                    <option value="На коришћењу">На коришћењу</option>
                    <option value="За расход">За расход</option>
                </select>
            </div>        
        </div>
        <!-- col 3 -->
        <div class="col-xs-3">
            <div class="form-group">
                {{ Form::text('serijski_broj', Input::old('serijski_broj'), array('class' => 'form-control', 'placeholder' => 'Серијски Број')) }}
            </div>
            <div class="form-group">
                <div class="input-group date" id="drzavni_zig">
                    {{ Form::text('drzavni_zig', Input::old('drzavni_zig'), array('class' => 'form-control', 'placeholder' => 'Датум државног жига')) }}
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </div>
            <div class="form-group">
                <select name="policijska_stanica_id" id="policijska_stanica_id" class="form-control">
                    <option>Izabrati policijsku stanicu</option>
                    @foreach ($pstanice as $pstanica) 
                        <option value="{{ $pstanica['id'] }}">{{ $pstanica['ime_pstanica'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!-- col 4 -->
        <div class="col-xs-3">
            <div class="form-group">
                {{ Form::text('inventarski_broj', Input::old('inventarski_broj'), array('class' => 'form-control', 'placeholder' => 'Инвентарски Број')) }}
            </div>         
            <div class="form-group">
                <div class="input-group">
                    {{ Form::text('troskovi_kalibracije', Input::old('troskovi_kalibracije'), array('class' => 'form-control', 'placeholder' => 'Трошкови Калибрације')) }}
                    <div class="input-group-addon">Дин.</div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {{ Form::text('troskovi_servisa', Input::old('troskovi_servisa'), array('class' => 'form-control', 'placeholder' => 'Трошкови Сервиса')) }}
                    <div class="input-group-addon">Дин.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                {{ Form::textarea('napomena', Input::old('napomena'), array('class' => 'form-control', 'placeholder' => 'Напомена', 'cols' => '30', 'rows' => '5')) }}
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <div class="form-group">
                        <div class="center-block">
                            {{ Form::submit('Додати', array('class' => 'btn btn-lg btn-success btn-block')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <!-- Modal ajax loader -->
    <div class="modal fade bs-example-modal-sm" id="ajaxModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <h3 class="text-center">{{ HTML::image('assets/img/spinner.gif') }} Учитавање</h3> 
                </div>
            </div>
        </div>
    </div>
@stop