@extends('main')

@section('content')
    <!-- Table results -->
    <div class="row">
        <div class="col-xs-12">
         <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('alkometri') }}">Алкометри</a></li>
            <li><a href="{{ URL::to('alkometri/proizvodjaci') }}">Произвођачи</a></li>
            <li class="active">{{ $proizvodjac->ime_alkometra }}</li>
        </ol> 

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endif

        @foreach( $errors->all() as $val)
            <p class="alert alert-warning">{{ $val }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </p>
        @endforeach  
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-heading">Изменити име произвођача</div>
                <div class="panel-body">
                    {{ Form::open(array('action' => array('AlkometriController@updateProizvodjac', $proizvodjac->id), 'method' => 'put')) }}
                    
                    <fieldset>
                        <div class="form-group">
                            <input type="text" name="ime_alkometra" class="form-control" value="{{ $proizvodjac->ime_alkometra }}">
                        </div>
                        {{ Form::submit('Измени', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-heading">Додати модел</div>
                <div class="panel-body">
                    {{ Form::open() }}
                    <fieldset>
                        <div class="form-group">
                            {{ Form::text('model', Input::old('model'), array('class' => 'form-control', 'placeholder' => 'Име модела', 'autofocus')) }}
                        </div>
                        {{ Form::submit('Додај', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <table class="table">
                <tr>
                    <th>Листа модела</th>
                </tr>
                @foreach ($modeli as $model)
                    <tr>
                        <td>{{ $model->model }} </td>
                    </tr>
                @endforeach
            </table>
            
        </div>
    </div>
@stop