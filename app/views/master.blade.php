<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=windows-1251">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        @section('title')
           Евиденција
        @show
    </title>

    <!-- CSS -->
    {{ HTML::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
    {{ HTML::style('assets/bower_components/metisMenu/dist/metisMenu.min.css') }}
    {{ HTML::style('assets/css/sb-admin-2.css') }}
    {{ HTML::style('assets/bower_components/font-awesome/css/font-awesome.min.css') }}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    {{ HTML::script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}
    <![endif]-->

</head>
<body>
    <div id="wrapper">
    <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::to('pretraga') }}">Евиденција</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ URL::to('user/'. Session::get('uid')) }}"><i class="fa fa-user fa-fw"></i> Кориснички профил</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ URL::to('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Излогуј се</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-search fa-fw"></i> Претрага<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#"> Возила</a>
                                </li>
                                <li>
                                    <a href="#"> Радари</a>
                                </li>
                                <li>
                                    <a href="#"> ИТ Опрема</a>
                                </li>
                                <li>
                                    <a href="#"> Алкометри</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> Преглед<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#"> Возила</a>
                                </li>
                                <li>
                                    <a href="#"> Радари</a>
                                </li>
                                <li>
                                    <a href="#"> ИТ Опрема</a>
                                </li>
                                <li>
                                    <a href="#"> Алкометри</a>
                                </li>
                            </ul>
                        </li>
                        @if (Session::has('sudo'))
                        <li>
                            <a href="#"><i class="fa fa-fw fa-cog"></i> Подешавања<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                                <li><a href="{{ URL::to('vozila') }}"> Возила</a></li>
                                <li><a href="#"> Радари</a></li>
                                <li><a href="#"> ИТ Опрема</a></li>
                                <li><a href="{{ URL::to('alkometri') }}"> Алкометри</a></li>
                                <li><a href="{{ URL::to('user') }}"> Корисници</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            @yield('content')
        </div>
        

    </div>
    <!-- /#wrapper -->
<!-- JavaScript -->
{{ HTML::script('assets/bower_components/jquery/dist/jquery.min.js') }}
{{ HTML::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}
{{ HTML::script('assets/bower_components/metisMenu/dist/metisMenu.min.js') }}
{{ HTML::script('assets/bower_components/morrisjs/morris.min.js') }}
{{ HTML::script('assets/js/sb-admin-2.js') }}

</body>

</html>