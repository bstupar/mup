<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content>
		<meta name="author" content>
		<link rel="icon" href="favicon.ico">
		<title>Template</title>

		<!-- CSS -->
    	{{ HTML::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
    	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->

		<style>
			body {
				padding-top: 70px;
				padding-bottom: 20px;
			}
		</style>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    <div class="container-fluid">
    	<!-- Filters -->
    	<div class="row">
    		<div class="col-xs-12">
    			
    			{{ Form::open( array('url' => 'vozila/pretraga')) }}

    			<fieldset>
    				<div class="form-group">
    					{{ Form::label('inventarski_br', 'Inventarski br.')}}
    					{{ Form::number('inventarski_br', Input::old('inventarski_br'), array('class' => 'form-control', 'placeholder' => 'Inventarski br.')) }}
    				</div>
    				<div class="form-group">
    					{{ Form::label('registarski_br', 'Registarski br.')}}
    					{{ Form::text('registarski_br', Input::old('registarski_br'), array('class' => 'form-control', 'placeholder' => 'Registarski br.' )) }}
    					
    				</div>
					<div class="form-group">
						{{ Form::label('marke_vozila', 'Marka vozila')}}
						{{ Form::select('marke_vozila', array('1' => 'Fiat', '2' => 'BMW', '3' => 'Jeep')) }}
						
					</div>
    			</fieldset>

    			{{ Form::close() }}

    		</div>
    	</div>
    	<!-- Table results -->
    	<div class="row">
    		<div class="col-xs-12">
    			<table class="table table-striped">
					<thead>
						<tr>
							<th>Бр.</th>
							<th>Инвентарски бр.</th>
							<th>Регистарски бр.</th>
							<th>Марка возила</th>
							<th>Тип возила</th>
							<th>Годиште</th>
							<th>Шасија</th>
							<th>Врста возила</th>
							<th>Обележије</th>
							<th>Почетна Км</th>
							<th>Крајња Км</th>
							<th>Стање</th>
							<th>Задужење</th>
							<th>Статус</th>
							<th>Напомена</th>
							<th>Пол. Станица</th>
						</tr>
					</thead>
					<tbody>
					<tr>
						<td class="col-md-2">1</td>
						<td class="col-md-2">22</td>
						<td class="col-md-2">BG 340 LI</td>
						<td class="col-md-2">Škoda Fabia</td>
						<td class="col-md-2">Putničko</td>
						<td class="col-md-2">2008</td>
						<td class="col-md-2">TD2335700SWWX895</td>
						<td class="col-md-2">Privatno</td>
						<td class="col-md-2">Nema</td>
						<td class="col-md-2">130000</td>
						<td class="col-md-2">133330</td>
						<td class="col-md-2">25</td>
						<td class="col-md-2">Boško Stupar</td>
						<td class="col-md-2">Aktivno</td>
						<td class="col-md-2">Privatno vozilo za sopstvene potrebe</td>
						<td class="col-md-2">Novi Beograd</td>
					</tr>
			
					</tbody>
				</table>
    		</div>
    	</div>
    </div>
		
	</body>
</html>